'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
  var util = require('util');
  const jsf = require('json-schema-faker');
  const chance = require('chance')
  const faker = require('faker')
  jsf.extend('chance', () => new chance.Chance());
  jsf.extend('faker', () => faker);

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  exhibition: getExhibition,
  comments: getReview,
  timetable: getTimetable,
};


var exhibition = {
  "type": "array",
  "minItems": 8,
  "maxItems": 8,
  "items": {
    "type": "object",
    "required": [
      "id", "name", "lastName", "description", "img1", "img2", "img3", "img4"
    ],
    "properties": {
      "id": {
        "type": "integer",
        "faker": "random.number"
      },
      "name": {
        "type": "string",
        "faker": "name.lastName"
      },
      "lastName": {
        "type": "string",
        "faker": "name.lastName"
      },
      "description": {
        "type": "string",
        "faker": "lorem.sentences"
      },
      "img1": {
        "type": "string",
        "faker": "image.imageUrl"
      },
      "img2": {
        "type": "string",
        "faker": "image.imageUrl"
      },
      "img3": {
        "type": "string",
        "faker": "image.imageUrl"
      },
      "img4": {
        "type": "string",
        "faker": "image.imageUrl"
      },
    }
  }
}


var reviewschema = {
  "type": "array",
  "minItems": 4,
  "maxItems": 4,
  "items": {
    "type": "object",
    "required": [
      "name", "comment", "id"
    ],
    "properties": {
      "id": {
        "type": "integer",
        "faker": "random.number"
      },
      "name": {
        "type": "string",
        "faker": "name.findName"
      },
      "comment": {
        "type": "string",
        "faker": "lorem.sentence"
      },
    }
  }
}

var timetable = {
  "type": "array",
  "minItems": 3,
  "maxItems": 3,
  "items": {
    "type": "object",
    "required": [
       "id", "place", "name", "adjective", "description", "number"
    ],
    "properties": {
      "id": {
        "type": "integer",
        "faker": "random.number"
      },
      "number": {
        "type": "integer",
        "faker": "random.number"
      },
      "place": {
        "type": "string",
        "faker": "address.streetAddress"
      },
      "adjective": {
        "type": "string",
        "faker": "company.bsAdjective"
      },
      "name": {
        "type": "string",
        "faker": "name.title"
      },
      "description": {
        "type": "string",
        "faker": "lorem.sentence"
      },
    }
  }
}
/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */

function getExhibition(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    var name = req.swagger.params.name.value || 'Van Gog';
   
  
    // this sends back a JSON response which is a single string
    jsf.resolve(exhibition).then(sample => res.json(sample));
  }


  function getReview(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    var name = req.swagger.params.name.value || 'stranger';
  
    // this sends back a JSON response which is a single string
    jsf.resolve(reviewschema).then(sample => res.json(sample));
  }

  function getTimetable(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    var name = req.swagger.params.name.value || 'meeting';
  
    // this sends back a JSON response which is a single string
    jsf.resolve(timetable).then(sample => res.json(sample));
  }