import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';



const api = new Api.DefaultApi()

class Comments extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            comments: []
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(comments) { //эта функция отвечает за обновление данных при перезагрузке страницы
        const response = await api.comments({ name: '' });
        this.setState({ comments: response });
    }

    render() {
        return <div>


			 <section class="reviews">

                <h3 class="title">What others say:</h3>
                {this.state.comments.map(
								(event) => 
                <div>

                    <p class="quote">{event.comment}.</p>
                    <p class="author">— {event.name}</p>

                </div>)
            }
            </section>
            <br></br>
            <br></br>
            <button class="button">
                <span class="button-text">View all &rarr;</span>
            </button>
        </div>


    }
}


export default Comments;