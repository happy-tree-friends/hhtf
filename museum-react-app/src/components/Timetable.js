import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';

const api = new Api.DefaultApi()

class Timetable extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            timetable: []
            
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(timetable) { 
        const response = await api.timetable({ name: '' });
        this.setState({ timetable: response });
    }

    render() {
        return <div>
           <section class="features">
		 <h3 class="title">Timetable</h3>
		 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id felis et ipsum bibendum ultrices. Morbi vitae pulvinar velit. Sed aliquam dictum sapien, id sagittis augue malesuada eu.</p>
		 <hr></hr>
         <ul class="grid">
                {this.state.timetable.map(
                        (event) => 
                        
                        <li>
                            <i class="fa fa-camera-retro"></i>
                            <h4>Exhibition from {event.name}</h4>
                            <p>{event.description}</p>
                            <p>Place: {event.place}</p>
                            <p>Booked tickets: {event.number}</p>
                            <br></br>
                            <br></br> 
                            <button class="button">
                                <span class="button-text"><a href="#contact">Buy ticket</a></span>
                            </button>
                        </li>     
            )

                }
                </ul>

    
        </section>
        <section id="contact" class="contact">
		<h3 class="title">Buy a ticket to the nearest exhibition</h3>	
		<hr />

		<form>
			<input type="tel" placeholder="+375 29 123-45-67" />
			<a href="#" class="btn">Buy</a>
		</form>
	    </section>
        
     </div>
    }
}

export default Timetable;
