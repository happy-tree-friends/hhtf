import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';



const api = new Api.DefaultApi()

class Exhibition extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            exhibition: []
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(exhibition) {
        const response = await api.exhibition({ name: '' });
        this.setState({ exhibition: response });
    }

    render() {
        return <div>
                    <h1 class="top" style={{color: '#4592FF'}}>Exhibited works:</h1>
                        {this.state.exhibition.map(
                            (event) => 
                                <div class="our-work">
                                        <section class="our-work">
                                            <div>
                                                <h3 class="title">{event.name} {event.lastName}</h3>
                                                <p class="author_description">{event.description}</p>
                                                <hr />

                                                <ul class="grid">
                                                    <li class="small" style={{backgroundImage: `url(${event.img1})`}}></li>
                                                    <li class="large" style={{backgroundImage: `url(${event.img2})`}}></li>
                                                    <li class="large" style={{backgroundImage: `url(${event.img3})`}}></li>
                                                    <li class="small" style={{backgroundImage: `url(${event.img4})`}}></li>
                                                    <button class="button">
                                                    <span class="button-text">View all &rarr;</span>
                                                </button>
                                                </ul>
                                            </div>
                                        </section>
                                </div>
                            )
                        }
			    </div>	
            }
}


export default Exhibition;