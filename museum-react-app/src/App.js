import './assets/css/styles.css';
import logo from './logo.svg';
import './App.css';
import React from 'react';
import Exhibition from './components/Exhibition';
import Comments from './components/Comments';
import Timetable from './components/Timetable';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,  
  useLocation
} from "react-router-dom";
import moment from 'moment'

function App() {
  return (

<Router>
    <div>

    <header>
      <h2><Link to="/">Night Museum</Link></h2>
        <nav>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/exhibition">Exhibition</Link></li>
          <li><Link to="/comments">Comments</Link></li>
          <li><Link to="/timetable">Timetable</Link></li>
        </nav>
	</header>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <div className="App">
        <div> 
          <Switch>
            <Route path="/exhibition">
              <Exhibition />
            </Route>
            <Route path="/timetable">
            <Timetable />
            </Route>
            
            <Route path="/comments">
              <Comments />
            </Route>

            <Route path="/">
              <div>
              <section class="hero">
                  <div class="background-image"></div>
                  <h1>Night Museum</h1>
                  <h3>Spend an awesome night with famous artists.</h3>
            </section>
            </div>
            </Route>
            <Route path="*">
              <NoMatch />
            </Route>
            
          </Switch>
          <footer>
                <ul>
                  <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                  <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                  <li><a href="#"><i class="fa fa-snapchat-square"></i></a></li>
                  <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                  <li><a href="#"><i class="fa fa-github-square"></i></a></li>
                </ul>
                <p>Made by the group "HHTF+"</p>
	        </footer>

        </div>
      </div>
    </div>
    </Router>
  );
}
function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}


 
export default App;

